package com.budggeter.service.security;

import com.budggeter.service.model.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {
    private UserService userService = new UserService();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            MyUserDetails myUserDetails = userService.getUserByName(username);
            return myUserDetails;
        } catch (Exception e) {
            throw new UsernameNotFoundException("Not found");
        }
    }
}
