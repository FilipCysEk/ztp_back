package com.budggeter.service.security;

public enum Role {
    ROLE_USER, ROLE_ADMIN
}
