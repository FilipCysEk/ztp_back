# Budggeter API

### Krzysztof Swałdek
#### 121115

Stworzone w wesji 11 javy.

## Uruchamianie

1) Uruchamianie za pomocą pliku .jar, który znajduje się w folderze out
    `java -jar budggeter2.jar`

2) Uruchamianie ze źródła. Plik `ServiceApplication.java` jest głównym plikiem z funkcją main.
Znajduje się w `src\main\java\com\budggeter\service`
    
