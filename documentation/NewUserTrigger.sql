BEGIN
DECLARE i INT DEFAULT 0;
DECLARE n INT DEFAULT 0;
DECLARE rvidCat INT DEFAULT 0;
DECLARE rvidCatIns INT DEFAULT 0;
DECLARE rvcname VARCHAR(50) DEFAULT "";
DECLARE rvcnameb VARCHAR(50) DEFAULT "";
DECLARE rvidcolor INT DEFAULT 0;
DECLARE rvisincome TINYINT(1) DEFAULT 0;
DECLARE rviddefsubcategory INT DEFAULT 0;
DECLARE rvidsubcategory INT DEFAULT 0;
DECLARE rvidsubcategoryIns INT DEFAULT 0;
DECLARE rvsname VARCHAR(50) DEFAULT "";

INSERT INTO money_account (name, id_user, visible) VALUES ("Usunięte konta", NEW.id_user, 0);
SET @last = (SELECT LAST_INSERT_ID());
INSERT INTO user_details (id_user, money_account_trash, rol) VALUES (NEW.id_user, @last, 0);

SELECT COUNT(*) FROM `category` JOIN subcategory ON category.id_category = subcategory.id_category WHERE id_user = 1 INTO n;

SET i = 0;

WHILE i < n DO
	SELECT category.id_category, category.name AS cname, id_color, is_income, id_def_subcategory, id_sub_cat, subcategory.name as sname
	FROM `category`
	JOIN subcategory ON category.id_category = subcategory.id_category
	WHERE id_user = 1
	LIMIT i,1
	INTO rvidCat, rvcname, rvidcolor, rvisincome, rviddefsubcategory, rvidsubcategory, rvsname ;

    IF rvcname = rvcnameb THEN
    	INSERT INTO subcategory (name, id_category) VALUES (rvsname, rvidCatIns);
        SELECT LAST_INSERT_ID() INTO rvidsubcategoryIns;
    ELSE
    	INSERT INTO category (name, id_user, id_color, is_income) VALUES (rvcname, NEW.id_user, rvidcolor, rvisincome);
        SELECT LAST_INSERT_ID() INTO rvidCatIns;
    	INSERT INTO subcategory (name, id_category) VALUES (rvsname, rvidCatIns);
        SELECT LAST_INSERT_ID() INTO rvidsubcategoryIns;
    end IF;

    IF rvidsubcategory = rviddefsubcategory THEN
    	UPDATE category SET id_def_subcategory = rvidsubcategoryIns WHERE id_category = rvidCatIns;
    END IF;

	SET rvcnameb = rvcname;
    SET i = i+1;

END WHILE;

END

BEGIN
	DECLARE trashAccount INT DEFAULT 0;
	SELECT money_account_trash
    FROM user_details
    WHERE id_user = OLD.id_user
    INTO trashAccount;

    UPDATE expense SET id_m_acc = trashAccount
    WHERE id_m_acc = OLD.id_m_acc;
END
